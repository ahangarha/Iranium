<?php
/**
 * Iranium tests
 * @package Iranium
 * @subpackage Tests
 */

/**
 * Iranium Tests class.
 */
class IraniumTest extends WP_UnitTestCase {
	/**
     * Check if all required files are there.
     */
    function test_includes() {
        $this->assertFileExists( IRANIUM_PLUGIN_DIR . 'includes/class-iridium.php' );
        $this->assertFileExists( IRANIUM_PLUGIN_DIR . 'includes/register-settings.php' );
        $this->assertFileExists( IRANIUM_PLUGIN_DIR . 'includes/hooks.php' );
        $this->assertFileExists( IRANIUM_PLUGIN_DIR . 'includes/plugins.php' );
        $this->assertFileExists( IRANIUM_PLUGIN_DIR . 'includes/class-logger77.php' );
    }
}

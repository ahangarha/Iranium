<?php
/**
 * Registers Iranium hooks & filters, where ever
 * they are needed.
 * 
 * @author ehsaan
 * @package Iranium
 * @subpackage Hooks
 */

/**
 * Applies Vazir font to TinyMCE editor, if enabled.
 * 
 * @since 1.0
 * @return void
 */
function iranium_editor_style() {
    global $iranium_options;
    if ( $iranium_options[ 'use_vazir_font' ] & 2 !== 0 ) {
        add_editor_style( IRANIUM_PLUGIN_URL . 'assets/css/editor-font.css' );
    }
}
add_filter( 'init', 'iranium_editor_style', 9 );

/**
 * Applies Vazir font to admin panel pages, if enabled.
 * 
 * @since 1.0
 * @return void
 */
function iranium_admin_style() {
    global $iranium_options;
    if ( $iranium_options[ 'use_vazir_font' ] & 1 !== 0 ) {
        wp_enqueue_style( 'vazir-font', IRANIUM_PLUGIN_URL . 'assets/css/pages-font.css', false, IRANIUM_VERSION, 'all' );
    }
}
add_action( 'admin_enqueue_scripts', 'iranium_admin_style' );

global $iranium_options;

if ( isset( $iranium_options[ 'use_iridium' ] ) && $iranium_options[ 'use_iridium' ] == 'enabled' ) {
    add_filter( 'the_date', 'iranium_post_date', 10, 2 );
    add_filter( 'get_comment_date', 'iranium_comment_date', 10, 2 );
    add_action( 'date_i18n', 'iranium_i18n', 10, 3 );
}

if ( isset( $iranium_options[ 'force_locale' ] ) && $iranium_options[ 'force_locale' ] == 'enabled' ) {
    add_filter( 'locale', 'iranium_set_locale', 0 );
}

/**
 * An alias of Iridium::persian_characters
 * 
 * @see Iridium::persian_characters
 * @param string $string
 * @return string
 */
function iranium_persian_characters( $string ) {
    return Iranium()->Iridium->persian_characters( $string );
}

/**
 * Force locale to fa_IR, if neccessary.
 * 
 * @param string $locale Current locale
 * @return string New locale
 */
function iranium_set_locale( $locale ) {
    global $locale;
    $locale = 'fa_IR';
    setlocale( LC_ALL, $locale );
    return $locale;
}

/**
 * Calculate post date in Iran calendar system.
 * This function is depreacted because most of
 * themes and plugins use i18n methods to display
 * the dates.
 * Therefore, this function is depreacted and
 * it is only here for backward compability.
 * 
 * @deprecated
 * @param string $time Post timestamp
 * @param string $format Output format (uses date_format option if not given)
 * @return string Datestamp in Persian calendar system.
 */
function iranium_post_date( $time, $format = '' ) {
    global $post;

    if ( ! $post ) return $time;
    if ( ! $format ) $format = get_option( 'date_format' );

    return Iranium()->Iridium->persian_date( $format, $post->post_date );
}

/**
 * Calculate comment date in Iran calendar system.
 * 
 * @param string $time Comment timestamp
 * @param string $format Output format (uses date_format option if not given)
 * @return string Datestamp in Persian calendar system.
 */
function iranium_comment_date( $time, $format = '' ) {
    global $comment;
    
    if ( ! $comment ) return $time;
    if ( ! $format ) $format = get_option( 'date_format' );

    return Iranium()->Iridium->persian_date( $format, $comment->comment_date );
}

/**
 * Calls when I18N dates are going to be formatted.
 * 
 * @param $j Formatted date string
 * @param $format Format to display the date.
 * @param $timestamp Unix timestamp.
 * @return string
 */
function iranium_i18n( $j, $format, $timestamp ) {
    /**
     * Check if we're being called by a plug-in that
     * shouldn't have used i18n function, e.g.: WooCommerce
     * 
     * @see plugins.php
     */
    $blocked_by_plugin = apply_filters( 'iranium_block_i18n_hook', $j, $format, $timestamp );
    if ( true === $blocked_by_plugin ) {
        /**
         * If date_i18n is called by a known plug-in
         * that shouldn't have called this function,
         * we stop the replacement function and continue
         * using the original date.
         */
        return $j;
    } else {
        /**
         * Green light is on, so we can convert the date.
         */
        global $locale;

        $converted = Iranium()->Iridium->persian_date( $format, $timestamp );

        /**
         * Check if the locale is fa_IR. If it's
         * true, use Persian digits convertor.
         */
        if ( 'fa_IR' === $locale ) {
            return Iranium()->Iridium->persian_digits( $converted );
        } else {
            return $converted;
        }
    }
}

/**
 * Instead of converting characters,
 * we'll give hints to users about
 * standard keyboard layouts. This
 * function will only work if user
 * locale is set to fa_IR.
 * 
 * @return void
 */
function iranium_keyboard_hint() {
    $screen = get_current_screen();

    if ( 'edit' === $screen->parent_base ):
        
        /**
         * Show the hint only on
         * edit.php pages.
         */
        
         /**
          * Check if user hasn't
          * dismissed the hint.
          */
        if ( 'dismissed' !== get_option( 'iranium_keyboard_hint_' . get_current_user_id(), null ) ) {
            
            /**
             * Check for WordPress locale.
             */
            if ( 'fa_IR' === get_locale() ) {
                /**
                 * We can show the hint.
                 */
                echo '<div class="updated notice is-dismissible"><p>';
                echo sprintf( __( '<strong>Iranium says:</strong> Using standard Persian keyboard layout for proper typing proper characters faster. <a href="%1$s" target="_blank">More info</a> &ndash; <a href="%2$s">Don\'t show me this again.</a>', 'iranium' ),
                    'https://iranium.gitlab.io/fa/keyboard.html',
                    add_query_arg( [ 'iranium_dismiss' => true ] )
                );
                echo '</p><button type="button" class="notice-dismiss"></button></div>';
            }
        }
    
    endif;
}
add_action( 'admin_notices', 'iranium_keyboard_hint' );

/**
 * Dismiss the hint if user
 * tells us to.
 * 
 * @return void
 */
function iranium_dismiss_hint() {
    if ( isset( $_GET[ 'iranium_dismiss' ] ) ) {
        if ( current_user_can( 'edit_posts' ) )
            update_option( 'iranium_keyboard_hint_' . get_current_user_id(), 'dismissed' );
        
        /**
         * Redirect back.
         */
        wp_safe_redirect( add_query_arg( [ 'iranium_dismiss' => false ] ) );
    }
}
add_action( 'admin_init', 'iranium_dismiss_hint' );
<?php
/**
 * Blocked Plugins.
 * If activating Iranium or another plug-in causes confliction
 * and probably the "White Screen of Death", you can put that
 * plug-in name here.
 * 
 * Example:
 * $blocked_plugins = array(
 *      'plugin-directory/plugin-file.php',
 *      'woocommerce/woocommerce.php'
 * );
 */

if ( ! defined( 'ABSPATH' ) ) exit;

global $blocked_plugins;
$blocked_plugins = array(
    /**
     * Put the plug-ins names here. See above for example.
     */
    // 'plugin-directory/plugin-file.php',
);
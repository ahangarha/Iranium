<?php
/**
 * Plugin Name: Iranium
 * Plugin URI: https://gitlab.com/Iranium
 * Description: <a href="https://en.wikipedia.org/wiki/Iranian_calendars" target="_blank">Persian calendar system</a> for WordPress.
 * Author: ehsaan
 * Author URI: https://ehsaan.me/
 * Version: 0.3.4
 * Text Domain: iranium
 * Domain Path: languages
 * 
 * Iranium is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *
 * Iranium is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Iranium. If not, see <http://www.gnu.org/licenses/>.
 * 
 * @package Iranium
 * @category Core
 * @author ehsaan (ehsaan@riseup.net)
 * @version 1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'IraniumClass' ) ):

/**
 * Main Iranium Class.
 *
 * @since 1.0
 */
final class IraniumClass {
    /**
     * Singleton pattern here :).
     * 
     * @var IraniumClass The one true Iranium
     * @since 1.0
     */
    private static $instance;

    /**
     * Iridium Object.
     * 
     * @var object|Iridium
     * @since 1.0
     */
	public $Iridium;
	
	/**
	 * Logger77 Object.
	 * 
	 * @var object|Logger77
	 * @since 1.0
	 */
	public $Logger;

    /**
     * Main Iranium Instance.
     * 
     * Insures that only one instance of Iranium exists in memory at any one
     * time. Also prevents needing to define globals all over the place.
     * 
     * @since 1.0
     * @static
     * @staticvar array $instance
     * @uses Iranium::setup_constants() Setup the constants needed.
     * @uses Iranium::includes() Include the required files.
     * @uses Iranium::load_textdomain() Load the language file.
     * @see Iranium()
     * @return object|Iranium The one true IraniumClass
     */
    public static function instance() {
        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof IraniumClass ) ) {
            self::$instance = new IraniumClass;
            self::$instance->setup_constants();

			add_action( 'plugins_loaded', [ self::$instance, 'load_textdomain' ], 1 );
			add_action( 'plugins_loaded', [ self::$instance, 'post_load_textdomain' ], 10 );

			self::$instance->includes();
			self::$instance->Logger = new Logger77;
        }
        
        return self::$instance;
    }

    /**
	 * Throw error on object clone.
	 *
	 * The whole idea of the singleton design pattern is that there is a single
	 * object therefore, we don't want the object to be cloned.
	 *
	 * @since 1.0
	 * @access protected
	 * @return void
	 */
	public function __clone() {
		// Cloning instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'iranium' ), '1.0' );
	}

	/**
	 * Disable unserializing of the class.
	 *
	 * @since 1.0
	 * @access protected
	 * @return void
	 */
	public function __wakeup() {
		// Unserializing instances of the class is forbidden.
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?', 'iranium' ), '1.0' );
    }
    
    /**
	 * Setup plugin constants.
	 *
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	private function setup_constants() {

		// Plugin version.
		if ( ! defined( 'IRANIUM_VERSION' ) ) {
			define( 'IRANIUM_VERSION', '0.3.4-beta' );
		}

		// Plugin Folder Path.
		if ( ! defined( 'IRANIUM_PLUGIN_DIR' ) ) {
			define( 'IRANIUM_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
		}

		// Plugin Folder URL.
		if ( ! defined( 'IRANIUM_PLUGIN_URL' ) ) {
			define( 'IRANIUM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
		}

		// Plugin Root File.
		if ( ! defined( 'IRANIUM_PLUGIN_FILE' ) ) {
			define( 'IRANIUM_PLUGIN_FILE', __FILE__ );
		}
    }
    
    /**
	 * Include required files.
	 * 
	 * @access private
	 * @since 1.0
	 * @return void
	 */
	private function includes() {
		global $iranium_options;

		require_once IRANIUM_PLUGIN_DIR . 'includes/install.php';

		require_once IRANIUM_PLUGIN_DIR . 'includes/register-settings.php';
		$iranium_options = iranium_get_settings();

		require_once IRANIUM_PLUGIN_DIR . 'includes/class-logger77.php';
		require_once IRANIUM_PLUGIN_DIR . 'includes/class-iridium.php';
		require_once IRANIUM_PLUGIN_DIR . 'includes/hooks.php';
		require_once IRANIUM_PLUGIN_DIR . 'includes/plugins.php';
	}

	/**
	 * Loads the plugin language files.
	 * 
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function load_textdomain() {
		global $wp_version;

		/*
		 * Due to the introduction of language packs through translate.wordpress.org, loading our textdomain is complex.
		 *
		 * To support existing translation files from before the change, we must look for translation files in several places and under several names.
		 *
		 * - wp-content/languages/plugins/iranium (introduced with language packs)
		 * - wp-content/plugins/iranium/languages/
		 *
		 * In wp-content/languages/iranium/ we must look for "iranium-{lang}_{country}.mo"
		 * In wp-content/plugins/easy-digital-downloads/languages/, we must look for both naming conventions. This is done by filtering "load_textdomain_mofile"
		 *
		 */
		// Set filter for plugin's languages directory.
		$iranium_lang_dir 	=	dirname( plugin_basename( IRANIUM_PLUGIN_FILE ) ) . '/languages/';
		$iranium_lang_dir	=	apply_filters( 'iranium_languages_directory', $iranium_lang_dir );

		// Traditional WordPress plugin locale filter.
		$get_locale = get_locale();
		if ( $wp_version >= 4.7 ) {
			$get_locale = get_user_locale();
		}

		/**
		 * Defines the plugin language locale used in the plugin.
		 * 
		 * @var $get_locale The locale to use. Uses `get_user_locale()` in WordPress 4.7 or later,
		 * 		otherwise uses `get_locale()`
		 */
		$locale 			=	apply_filters( 'plugin_locale', $get_locale, 'iranium' );
		$mofile 			=	sprintf( '%1$s-%2$s.mo', 'iranium', $locale );

		// Look for wp-content/languages/iranium/iranium-{lang}_{country}.mo
		$mofile_global		=	WP_LANG_DIR . '/iranium/' . $mofile;

		if ( file_exists( $mofile_global ) ) {
			load_textdomain( 'iranium', $mofile_global );
		} else {
			// Load the default plugin language files.
			load_plugin_textdomain( 'iranium', false, $iranium_lang_dir );
		}
	}

	/**
	 * Post load_textdomain function.
	 * It loads Iridium into the memory.
	 * 
	 * @access public
	 * @since 1.0
	 * @return void
	 */
	public function post_load_textdomain() {
		self::$instance->Iridium = new Iridium;
		self::$instance->Iridium->bootstrap();
	}
}

endif; // End if class_exists check.

/**
 * The main function for that returns IraniumClass.
 * 
 * The main function responsible for returning the one true
 * IraniumClass Instance to functions everywhere.
 * 
 * Use this function like you would a global variable, except without needing
 * to declare the global.
 * 
 * Example: <?php $iranium = Iranium(); ?>
 * 
 * @since 1.0
 * @return object|Iranium The one true IraniumClass instance.
 */
function Iranium() {
	return IraniumClass::instance();
}

// Get Iranium running.
Iranium();